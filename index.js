const app = require('express')()
const bodyParser = require('./node_modules/body-parser')
app.validator = require('./service/validator')
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/task', (req, res, next) => {
    try {
        return res.status(200).send('Sucesso!')
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.get('/task/:id', (req, res, next) => {
    try {
        const id = req.params.id
        return res.status(200).send('Sucesso!')
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.post('/task', (req, res, next) => {
    try {
        const task = {
            name: req.body.name,
            description: req.body.description
        }
        app.validator.existsOrError(task.name, 'Nome do Task não informado!')
        return res.status(200).send('Task criada com sucesso!')
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.put('/task/:id', (req, res, next) => {
    try {
        const task = {
            id: req.params.id,
            name: req.body.name,
            description: req.body.description
        }
        app.validator.existsOrError(task.name, 'Nome do Task não informado!')
        return res.status(200).send(`Task ${task.id} atualizada com sucesso!`)
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.delete('/task/:id', (req, res, next) => {
    try {
        const id = req.params.id
        return res.status(200).send(`Task ${id} apagada com sucesso!`)
    } catch (error) {
        return res.status(500).send(error)
    }
})

app.listen(3003, () => {
    console.log(`Servidor está executando Task na porta 3003.`)
})