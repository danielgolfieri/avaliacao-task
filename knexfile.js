module.exports = {
  client: 'mysql2',
  connection: {
    host: 'localhost',
    database: 'db_task',
    user: 'user_task',
    password: '123456'
  },
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    tableName: 'knex_migrations'
  }
};
