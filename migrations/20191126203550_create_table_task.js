
exports.up = function (knex) {
    return knex.schema.createTable('task', table => {
        table.increments('id').primary()
        table.string('name').notNull()
        table.string('description', 1000)
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('task')
};
