# AVALIAÇÃO - TASK

## Pre-requisites:
1. MySQL
1.1. Create schema db_task
2. Postman

## Setup
Step 1: We need to clone the project.

```Terminal
$ cd ~/avaliacao-task # or whatever directory you keep your projects in
$ git clone git@bitbucket.org:danielgolfieri/avaliacao-task.git

Step 2: we meed to install the packages

```Terminal
$ npm install

step 3: we need to configure db

$ knex init
$ knex migrate:latest

## Run API
````Terminal
$ npm start
